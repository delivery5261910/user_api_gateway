package client

import (
	"user_api_gateway/config"
	pbc "user_api_gateway/genproto/user_service"
	"google.golang.org/grpc"
)

type IServiceManger interface {
	ClientService() pbc.ClientServiceClient
	BranchService() pbc.BranchServiceClient
}

type grpcClients struct {
	clientService pbc.ClientServiceClient
	branchService  pbc.BranchServiceClient
}

func NewGrpcClients(cfg config.Config) (IServiceManger, error) {
	connClientService, err := grpc.Dial(
		cfg.AdminGRPCServiceHost+cfg.AdminGRPCServicePort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		clientService: pbc.NewClientServiceClient(connClientService),
		branchService:  pbc.NewBranchServiceClient(connClientService),
	}, nil
}

func (g *grpcClients) ClientService() pbc.ClientServiceClient {
	return g.clientService
}

func (g *grpcClients) BranchService() pbc.BranchServiceClient {
	return g.branchService
}
