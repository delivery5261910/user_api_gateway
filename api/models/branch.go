package models

type Branch struct {
	ID             string `json:"id"`
	Name           string `json:"name"`
	Phone          string `json:"phone"`
	DeliveryTariff string `json:"delivery_tariff"`
	WorkHour       string `json:"work_hour"`
	Address        string `json:"address"`
	Destination    string `json:"destination"`
	Status         bool   `json:"status"`
	CreatedAt      string `json:"created_at"`
	UpdatedAt      string `json:"updated_at"`
	DeletedAt      int    `json:"deleted_at"`
}

type CreateBranch struct {
	Phone          string `json:"phone"`
	DeliveryTariff string `json:"delivery_tariff"`
	WorkHour       string `json:"work_hour"`
	Address        string `json:"address"`
	Destination    string `json:"destination"`
	Status         bool   `json:"status"`
}

type BranchResponse struct {
	Branches []Branch `json:"branches"`
	Count    int      `json:"count"`
}
