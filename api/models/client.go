package models

type CreateClient struct {
	FirstName          string `json:"first_name"`
	LastName           string `json:"last_name"`
	Phone              string `json:"phone"`
	DateOfBirth        string `json:"date_of_birth"`
}

type Client struct {
	ID                 string `json:"id"`
	FirstName          string `json:"first_name"`
	LastName           string `json:"last_name"`
	Phone              string `json:"phone"`
	DateOfBirth        string `json:"date_of_birth"`
	LastOrderedDate    string `json:"last_ordered_date"`
	TotalOrderedsSum   string `json:"total_orders_sum"`
	TotalOrderedsCount string `json:"total_orders_count"`
	DiscountType       string `json:"discount_type"`
	DiscountAmount     string `json:"discount_amount"`
	CreatedAt          string `json:"created_at"`
	UpdatedAt          string `json:"updated_at"`
	DeletedAt          int    `json:"deleted_at"`
}

type ClientResponse struct {
	Clients []Client `json:"clients"`
	Count    int       `json:"count"`
}
