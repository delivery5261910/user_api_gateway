package handler

import (
	pbu "user_api_gateway/genproto/user_service"
	"context"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
	"strconv"
	"time"
)

// CreateClient godoc
// @Router       /client [POST]
// @Summary      Create a new client
// @Description  Create a new client
// @Tags         client
// @Accept       json
// @Produce      json
// @Param        client body models.CreateClient false "client"
// @Success      201  {object}  models.Client
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateClient(c *gin.Context) {
	request := pbu.CreateClientRequest{}

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.ClientService().Create(ctx, &request)
	if err != nil {
		handleResponse(c, h.log, "error while creating client", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "customer created", http.StatusCreated, resp)
}

// GetClient godoc
// @Router       /client/{id} [GET]
// @Summary      Get client by id
// @Description  get client by id
// @Tags         client
// @Accept       json
// @Produce      json
// @Param 		 id path string true "client_id"
// @Success      200  {object}  models.Client
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetClient(c *gin.Context) {
	uid := c.Param("id")

	id, err := uuid.Parse(uid)
	if err != nil {
		handleResponse(c, h.log, "invalid uuid type", http.StatusBadRequest, err.Error())
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	client, err := h.services.ClientService().Get(ctx, &pbu.PrimaryKey{
		Id: id.String(),
	})
	if err != nil {
		handleResponse(c, h.log, "error while getting client by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, client)
}

// GetClientList godoc
// @Router       /clients [GET]
// @Summary      Get client list
// @Description  get client list
// @Tags         client
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        search query string false "search"
// @Success      201  {object}  models.ClientResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetClientList(c *gin.Context) {
	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	client, err := h.services.ClientService().GetList(context.Background(), &pbu.GetListRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})

	if err != nil {
		handleResponse(c, h.log, "error  while get list", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, client)
}

// UpdateClient godoc
// @Router       /client/{id} [PUT]
// @Summary      Update client
// @Description  update client
// @Tags         client
// @Accept       json
// @Produce      json
// @Param 		 id path string true "client_id"
// @Param        client body models.Client true "client"
// @Success      200  {object}  models.Client
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateClient(c *gin.Context) {
	updateclient := pbu.Client{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c, h.log, "invalid uuid", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	updateclient.Id = uid

	if err := c.ShouldBindJSON(&updateclient); err != nil {
		handleResponse(c, h.log, "error while reading body", http.StatusBadRequest, err.Error())
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	resp, err := h.services.ClientService().Update(ctx, &updateclient)
	if err != nil {
		handleResponse(c, h.log, "error while updating client", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, resp)
}

// DeleteClient godoc
// @Router       /client/{id} [DELETE]
// @Summary      Delete client
// @Description  delete client
// @Tags         client
// @Accept       json
// @Produce      json
// @Param 		 id path string true "client_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteClient(c *gin.Context) {
	uid := c.Param("id")
	id, err := uuid.Parse(uid)
	if err != nil {
		handleResponse(c, h.log, "uuid is not valid", http.StatusBadRequest, err.Error())
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	_, deleteErr := h.services.ClientService().Delete(ctx, &pbu.PrimaryKey{
		Id: id.String(),
	})
	if deleteErr != nil {
		handleResponse(c, h.log, "error while deleting client by id", http.StatusInternalServerError, deleteErr.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, "success deleting client data")
}
