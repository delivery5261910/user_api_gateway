package handler

import (
	"user_api_gateway/api/models"
	"user_api_gateway/config"
	"user_api_gateway/grpc/client"
	"user_api_gateway/pkg/logger"
	"github.com/gin-gonic/gin"
)

type Handler struct {
	cfg      config.Config
	log      logger.ILogger
	services client.IServiceManger
}

func New(cfg config.Config, log logger.ILogger, services client.IServiceManger) Handler {
	return Handler{
		cfg:      cfg,
		log:      log,
		services: services,
	}
}

func handleResponse(c *gin.Context, log logger.ILogger, msg string, statusCode int, data interface{}) {
	resp := models.Response{}

	switch code := statusCode; {
	case code < 400:
		resp.Description = "OK"
		log.Info("~~~~> OK", logger.String("msg", msg), logger.Any("status", code))
	case code == 401:
		resp.Description = "Unauthorized"
	case code < 500:
		resp.Description = "Bad Request"
		log.Error("!!!!! BAD REQUEST", logger.String("msg", msg), logger.Any("status", code))
	default:
		resp.Description = "Internal Server Error"
		log.Error("!!!!! INTERNAL SERVER ERROR", logger.String("msg", msg), logger.Any("status", code), logger.Any("error", data))
	}

	resp.StatusCode = statusCode
	resp.Data = data

	c.JSON(resp.StatusCode, resp)
}
